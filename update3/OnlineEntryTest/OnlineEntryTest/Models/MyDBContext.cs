﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace OnlineEntryTest.Models
{
    public class MyDBContext : DbContext
    {
        public DbSet<Users> Users
        {
            get;
            set;
        }

        public DbSet<Teachers> Teacher
        {
            get;
            set;
        }

        public DbSet<Test_Data> Test
        {
            get;
            set;
        }

        public DbSet<TestModule> TM
        {
            get;
            set;
        }

        public DbSet<LoginHist> LH
        {
            get;
            set;
        }

    }
}