﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineEntryTest.Models
{
    public static class AdminBal
    {

        public static bool SaveTest(DTO.Teachers tt)
        {

            using (Models.MyDBContext ctx = new Models.MyDBContext()) {
                ctx.Teacher.Add(tt);
                ctx.SaveChanges();

                return true;
            
            
            }

           
            return false;
        
        }

        public static List<DTO.Teachers> GetTest()
        {

            try
            {

                using (Models.MyDBContext ctx = new Models.MyDBContext())
                {
                    List<DTO.Teachers> tt = new List<DTO.Teachers>();
                    tt = ctx.Teacher.SqlQuery("Select * from dbo.Teachers").ToList();
                    ctx.SaveChanges();

                    return tt;


                }

            }
            catch(Exception ex)
            {

                    Console.WriteLine(ex.StackTrace);
                    List<DTO.Teachers> tt = new List<DTO.Teachers>();
                    return tt;

            }
                      

        }

        
    }
}