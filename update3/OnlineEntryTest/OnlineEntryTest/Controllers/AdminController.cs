﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlTypes;
namespace OnlineEntryTest.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult DashBoard()
        {
           
            return View();
        }

        //
        // GET: /Admin/Details/5
        [HttpGet]
        public ActionResult AddTest()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SaveTest()
        {
            
            DTO.Teachers tt = new DTO.Teachers();
            tt.TestName = Request["FullName"];
            tt.Address = Request["Address"];
            tt.ContactNumber = Request["Contact"];
            tt.Email = Request["Email"];
            tt.Password = Request["Password"];
            tt.TestOwner = Request["TestOwner"];
            int TestModules = int.Parse(Request["TestModule"]);
            SqlDateTime myDateTime = Convert.ToDateTime( Request["Date"]);
            tt.TestTiming = (DateTime)myDateTime;
               tt.Modules = new List<DTO.TestModule>();

               tt.LoginID = tt.TestName + tt.Email;  
           
            int i = 1;
            while (i <= TestModules)
            {

                string name = Request[""+i+""];
            

                tt.Modules.Add(new DTO.TestModule()
                {

                    ModuleName1 = name

                    

                });

                i++;
            
            }

            Models.AdminBal.SaveTest(tt);

            var route = new
            {
                controller = "Admin",
                action = "DashBoard"
            };

            Session["Save"] = "record saved";
            ViewBag.Save = "Record Saved SUCCESSFULLY";
            return RedirectToRoute(route);
                

          
        }
        public ActionResult DeleteTest()
        {
            List<DTO.Teachers> tt = Models.AdminBal.GetTest();
            return View(tt);
        }
        //
        // GET: /Admin/Create

        public ActionResult DeleteUser()
        {
            return View();
        }

        //
        // POST: /Admin/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/Edit/5

        public ActionResult SearchTest()
        {
            return View();
        }

        public ActionResult SearchUser()
        {
            return View();
        }

        //
        // POST: /Admin/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Admin/Delete/5

        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Admin/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
