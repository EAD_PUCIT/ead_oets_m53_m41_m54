﻿using System.Web;
using System.Web.Optimization;

namespace OnlineEntryTest
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
           
           
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        
                        "~/Scripts/js/jquery*",
                        "~/Scripts/js/slimbox2.js",
                        "~/Scripts/js/scroll-startstop.events.jquery.js", "~/Scripts/js/jquery-1.6.3.min.js"
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            
            
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/css/nivoslider.css",
                        "~/Content/css/slimbox2.css",
                        "~/Content/tooplate_style.css","~/Content/css/ddsmoothmenu.css"));
        }
    }
}