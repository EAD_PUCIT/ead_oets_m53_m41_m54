﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTO
{
    [Table ("Users")]
    public class Users
    {
        [Key]
        public int UserID {get ; set; }

        public string FullName { get; set; }

        public string Password { get; set; }

        public string Email {get ;  set;}
        
        public string CNIC { get; set; }

        public string DOB { get; set; }

        public string picture { get; set; }

        public int TestID { get; set; }

        public bool IsActive { get; set; }
        
        public int TotalMarks { get; set; }

        public string Address { get; set; }

        public String LoginID { get; set; }
        
        public virtual ICollection<LoginHist> Modules { get; set; }
    

    }
}
