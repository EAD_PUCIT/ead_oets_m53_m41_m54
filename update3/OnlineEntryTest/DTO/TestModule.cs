﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTO
{
    [Table ("TestModule") ]
    public class TestModule
    {
        [Key]
        public int TestModuleID { get; set; }
        
        public int TestID { get; set; }
        
        public string ModuleName1 { get; set; }

        public virtual Teachers Teacher { get; set; }

    }
}
