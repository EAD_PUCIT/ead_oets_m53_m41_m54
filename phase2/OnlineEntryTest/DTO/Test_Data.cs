﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTO
{
    [Table("Test_Data")]
    public class Test_Data
    {
        [Key]
        public int QuestionID { get; set; }

        public int TestID { get; set; }

        public int TestModuleID { get; set; }

        public string Statement { get; set; }

        public string Option1 { get; set; }

        public string Option2 { get; set; }

        public string Option3 { get; set; }

        public string Option4 { get; set; }

        public int CorrectOption { get; set; }
    }
}
