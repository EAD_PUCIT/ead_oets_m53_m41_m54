﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTO
{
    [Table("LoginHist")]
    public class LoginHist
    {
        [Key]
        public int LoginID { get; set; }

        public int UserID { get; set; }

        public DateTime LoginTime { get; set; }



        public virtual Users Users { get; set; }




    }
}
