﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace DTO
{
    [Table ("Teacher") ]
    public class Teacher
    {
        [Key]
        public int TestID { get; set; }

        public string TestName { get; set; }

        public string Address { get; set; }

        public string ContactNumber { get; set; }

        public string Password { get; set; }

        public string Email { set; get; }

        public DateTime TestTiming { set; get; }

        public string TestOwner { get; set; }

        public bool IsActive { get; set; }

        public virtual ICollection<TestModule> Modules { get; set; }
    
    
    }
}
